/* 
Connect a create the PostgreSQL database (gmt):

$ sudo -i -u postgres
$ createdb gtm
$ psql -d gtm
or
$ psql
=# \c gtm

A datatype recommendation for latitud and longitude values in:
    http://stackoverflow.com/questions/12504208/what-mysql-data-type-should-be-used-for-latitude-longitude-with-8-decimal-places

About the datatype for the e-mail as primary key in:
  http://dba.stackexchange.com/questions/68266/postgresql-datatype-for-email-address

For 'CITEXT' datatype, from terminal as root:

# apt-get install postgresql-contrib
# exit
$ psql
=# \c gtm
=# CREATE EXTENSION CITEXT;

A datatype recommendation for passwords in:
  http://stackoverflow.com/questions/1200326/what-is-the-datatype-for-a-password-in-postgresql
*/

CREATE TABLE member (
  id CITEXT PRIMARY KEY,
  first_name VARCHAR(31) NOT NULL,
  last_name VARCHAR(31) NOT NULL,
  run VARCHAR(15) NOT NULL,
  passwd VARCHAR(31) NOT NULL,
  is_admin BOOLEAN NOT NULL
);

CREATE TABLE citizen (
  id CITEXT PRIMARY KEY
);

CREATE TABLE container (
  id SERIAL PRIMARY KEY,
  type_of_materials VARCHAR(15) NOT NULL,
  lat DECIMAL(10, 8) NOT NULL,
  lng DECIMAL(11, 8) NOT NULL,
  description VARCHAR(255),
  filling_time TIME
);

CREATE TABLE container_time_serie (
  id SERIAL PRIMARY KEY,
  modification_date TIMESTAMP NOT NULL ,
  id_container INT NOT NULL,
  status VARCHAR(7) NOT NULL,
  id_member CITEXT NOT NULL, 
  FOREIGN KEY (id_container) REFERENCES container(id),
  FOREIGN KEY (id_member) REFERENCES member(id)
);

CREATE TABLE pending_container_list (
  id SERIAL PRIMARY KEY,
  notification_date TIMESTAMP NOT NULL ,
  id_container INT NOT NULL,
  notification_status VARCHAR(7) NOT NULL,
  id_notifier CITEXT NOT NULL,
  id_reviewer CITEXT,
  FOREIGN KEY (id_container) REFERENCES container(id),
  FOREIGN KEY (id_notifier) REFERENCES member(id),
  FOREIGN KEY (id_reviewer) REFERENCES member(id)
);

INSERT INTO member VALUES ('sir@williamwallace.com', 'William', 'Wallace',
                          '23.1305-9', 'freedom', TRUE);
INSERT INTO member VALUES ('ragnar@lothbrok.com', 'Ragnar', 'Lothbrok',
                          '75.5865-9', 'Odin!', FALSE);
INSERT INTO citizen VALUES ('jmarroquin@csrg.cl');
INSERT INTO container (type_of_materials, lat, lng)
       VALUES ('PET', -33.0363188, -71.5980542);
INSERT INTO container (type_of_materials, lat, lng)
       VALUES ('paper', -33.0322946, -71.5734713);
INSERT INTO container_time_serie (modification_date, id_container, status,
                                  id_member)
       VALUES ('3/5/2016 14:02:19', 1, 'empty', 'sir@williamwallace.com'),
              ('3/5/2016 19:03:25', 2, 'full', 'sir@williamwallace.com');
INSERT INTO pending_container_list (notification_date, id_container,
                                    notification_status, id_notifier)
       VALUES ('6/5/2016 12:19:03', 1, 'empty', 'jmarroquin@csrg.cl'),
              ('8/5/2016 13:25:02', 2, 'full', 'rosa.llanos.13@sansano.usm.cl'),
              ('8/5/2016 13:25:02', 1, 'full', 'hernan.valencia.13@sansano.usm.cl');